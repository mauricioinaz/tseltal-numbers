#from os import system
NUMBERS_1_20 = [
    "jun",
    "cheb",
    "oxeb",
    "chaneb",
    "ho'eb",
    "waqueb",
    "huqueb",
    "waxaqueb",
    "baluneb",
    "lajuneb",
    "buhlucheb",
    "lajchayeb",
    "oxlajuneb",
    "chanlajuneb",
    "ho'lajuneb",
    "waclajuneb",
    "huclajuneb",
    "waxaclajuneb",
    "balunlajuneb"
    ]

MULTP_1_20 = [
    "", "j", "cha'", "ox", "chan", "ho'", "wac", "huc", "waxac", "balun",
    "lajun", "buhluch", "lajchay","oxlajun", "chanlajun", "ho'lajun",
    "waclajun", "huclajun", "waxaclajun", "balunlajun"]

PRONOUNS_1_20 = [
    "",
    "j",
    "s",
    "y",
    "s",
    "s",
    "s",
    "s",
    "s",
    "s",
    "s",
    "s",
    "s",
    "y",
    "s",
    "s",
    "s",
    "s",
    "s",
    "s",
    ]

ORDERS = ["", "winic", "bahc'", "pic", "bahc' bahqu'etic", "mam"]
#Other regions use jcalab insteaf of jbahc' bahqu'etic

def say(number):

    #CHECK FOR DIGITS!!!
    if 0 > number < 63999999 :
        raise ValueError("Name out of range / yahtabal c'ax muc'")

    #EXCEPTIONS
    if number == 0:
        return "ma'yuc"

    length = 0
    while number // (20 ** length) != 0:
        length += 1

    list_units = [(number % 20 ** x) // 20 ** (x-1) for x in range(1,length + 1)]
    print(list_units)
    
    i = 0
    result = ""
    flag = False
    for unit in list_units:
        if i == 0 and 0 != unit:
            print('1 if UNITS')
            result = NUMBERS_1_20[unit - 1]
        elif unit == 0:
            if i == 0:
                print('2 elif ==0')
                flag = True
        elif unit == 19 and not(flag):
            print('3 elif == 19')
            result += " s" + ORDERS[i + 1] + "al"
        else:
            if flag:
                if unit != 0:
                    if unit == 1 and i == 1:
                        print('4 else FLAG TAHB')
                        flag = False
                        result += "jtahb"
                    else:
                        print('4 else FLAG')
                        flag = False
                        result += MULTP_1_20[unit] + ORDERS[i]
            else:
                print('5 ELSE')
                result += " " + PRONOUNS_1_20[unit + 1] + MULTP_1_20[unit + 1] + ORDERS[i]
        i += 1

    return result
        #raise ValueError("Out of Range! For now only from 1 to billion")
