import unittest

from say_TSELTAL import say


# Tests adapted from `problem-specifications//canonical-data.json` @ v1.1.0

class SayTest(unittest.TestCase):
    def test_zero(self):
        self.assertEqual(say(0), "ma'yuc")

    def test_one(self):
        self.assertEqual(say(1), "jun")

    def test_fourteen(self):
        self.assertEqual(say(14), "chanlajuneb")

    def test_twenty(self):
        self.assertEqual(say(20), "jtahb")

    def test_twenty_two(self):
        self.assertEqual(say(22), "cheb scha'winic")

    def test_one_hundred(self):
        self.assertEqual(say(100), "ho'winic")

    # additional track specific test
    def test_one_hundred_twenty(self):
        self.assertEqual(say(120), "wacwinic")

    def test_one_hundred_twenty_three(self):
        self.assertEqual(say(123), "oxeb shucwinic")

    def test_three_hundred_seventy_nine(self):
        self.assertEqual(say(379), "balunlajuneb sbalunlajunwinic")

    def test_three_hundred_eighty(self):
        self.assertEqual(say(380), "balunlajunwinic")

    def test_three_hundred_eighty_one(self):
        self.assertEqual(say(381), "jun sbahc'al")

    def test_fourhundred(self):
        self.assertEqual(say(400), "jbahc'")

    def test_fourhundred_and_one(self):
        self.assertEqual(say(401), "jun scha'bahc'")

    def test_pic0(self):
        self.assertEqual(say(420), "jtahb scha'bahc'")

    def test_pic1(self):
        self.assertEqual(say(421), "jun scha'winic scha'bahc'")

    def test_pic2(self):
        self.assertEqual(say(800), "cha'bahc'")

####FALLA
    def test_pica(self):
        self.assertEqual(say(7599), "balunlajuneb sbalunlajunwinic sbalunlajunbahc'")

    def test_picb(self):
        self.assertEqual(say(7600), "balunlajunbahc'")

    def test_picc(self):
        self.assertEqual(say(7601), "jun spical")

    def test_picd(self):
        self.assertEqual(say(8000), "jpic")

    def test_pice(self):
        self.assertEqual(say(8001), "jun scha'pic")

    def test_picd(self):
        self.assertEqual(say(8020), "jtahb scha'pic")

###bahc' bahqu'etic
    def test_bbahc0(self):
        self.assertEqual(say(151999), "balunlajuneb sbahcal balunlajunbahc sbalunlajunpic")

    def test_bbahc1(self):
        self.assertEqual(say(152000), "balunlajunpic")

    def test_bbahc2(self):
        self.assertEqual(say(152001), "jun sbahc' bahqu'etical")

    def test_bbahc3(self):
        self.assertEqual(say(159599), "balunlajuneb sbalunlajunwinic sbalunlajunbahc' sjbahc' bahqu'etical")

    def test_bbahca(self):
        self.assertEqual(say(160000), "jbahc' bahqu'etic")

    def test_bbahcb(self):
        self.assertEqual(say(160001), "jun scha'bahc' bahqu'etic")

###MAM
    def test_mama(self):
        self.assertEqual(say(3200000), "jmam")

    def test_mamb(self):
        self.assertEqual(say(3200001), "jun scha'mam")

    # Utility functions
    def setUp(self):
        try:
            self.assertRaisesRegex
        except AttributeError:
            self.assertRaisesRegex = self.assertRaisesRegexp

    def assertRaisesWithMessage(self, exception):
        return self.assertRaisesRegex(exception, r".+")


if __name__ == '__main__':
    unittest.main()


"""
    def test_one_thousand(self):
        self.assertEqual(say(1000), "one thousand")

    def test_one_thousand_two_hundred_thirty_four(self):
        self.assertEqual(say(1234), "one thousand two hundred and thirty-four")

    # additional track specific test
    def test_eight_hundred_and_ten_thousand(self):
        self.assertEqual(say(810000), "eight hundred and ten thousand")

    def test_one_million(self):
        self.assertEqual(say(1e6), "one million")

    # additional track specific test
    def test_one_million_two(self):
        self.assertEqual(say(1000002), "one million and two")

    def test_1002345(self):
        self.assertEqual(
            say(1002345),
            "one million two thousand three hundred and forty-five")

    def test_one_billion(self):
        self.assertEqual(say(1e9), "one billion")

    def test_987654321123(self):
        self.assertEqual(
            say(987654321123), ("nine hundred and eighty-seven billion "
                                "six hundred and fifty-four million "
                                "three hundred and twenty-one thousand "
                                "one hundred and twenty-three"))

    def test_number_too_large(self):
        with self.assertRaisesWithMessage(ValueError):
            say(1e12)

    def test_number_negative(self):
        with self.assertRaisesWithMessage(ValueError):
            say(-1)
"""
